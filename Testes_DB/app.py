# coding: utf-8
from flask import Flask, render_template, request, redirect
from UsuariosBD import Usuarios
import pymysql

# import sqlite3

app = Flask(__name__)


@app.route('/')
def listaUsuarios():
    user = Usuarios()

    res = user.selectUserALL()

    return render_template('formListaTeste.html', result=res, content_type='application/json')


@app.route('/addUser', methods=['POST'])
def addUser():
    user = Usuarios()

    # user.id_usuario = request.form['id_usuario']
    user.nome = request.form['nome']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']

    executou = user.insertUser()
    print(executou)

    return redirect('/')


@app.route('/formEditUsuario', methods=['POST'])
def formEditUser():
    user = Usuarios()

    # realiza a busca pelo usuario e armazena o resultado no objeto
    executou = user.selectUser(request.form['id_usuario'])
    print(executou)

    return render_template('formTeste.html', user=user, content_type='application/json')


@app.route('/editUser', methods=['POST'])
def editUser():
    user = Usuarios()

    user.id_usuario = request.form['id_usuario']
    user.nome = request.form['nome']
    user.login = request.form['login']
    user.senha = request.form['senha']
    user.grupo = request.form['grupo']

    if 'salvaEditaClienteDB' in request.form:
        user.updateUser()
    elif 'excluiClienteDB' in request.form:
        user.deleteUser()

    return redirect('/')


if __name__ == "__main__":
    app.run()
